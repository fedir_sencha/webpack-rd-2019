const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
         exclude: /node_modules/,
         use: {
           loader: 'babel-loader',
           options: {
             presets: ['@babel/preset-env']
           }
         }
       },
       {
         test: /\.scss$/,
         exclude: /node_modules/,
         use: [
           {
             loader: 'style-loader',
           },
           {
             loader: 'css-loader',
           },
           {
             loader: 'sass-loader',
           }
         ],
       },
       {
         test: /\.(eot|svg|ttf|woff|woff2)$/,
         exclude: /node_modules/,
         use: {
           loader: 'file-loader',
           options: {
             outputPath: 'fonts/',
           },
         }
       },
       {
         test: /\.(png|svg|jpg|gif)$/,
         use: {
           loader: 'file-loader',
           options: {
             outputPath: 'assets/',
           }
         },
       }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './index.html',
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    open: true,
  }
};
