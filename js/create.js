export const createPost = (imageUrl, factData) => {
  const post = document.createElement('div');
  const imageContainer = document.createElement('div');
  const image = document.createElement('img');
  const fact = document.createElement('div');
  const factHeading = document.createElement('h3');

  post.classList.add('post');
  imageContainer.classList.add('post_image');
  fact.classList.add('fact');
  factHeading.classList.add('heading');
  image.classList.add('image');
  image.src = imageUrl;
  factHeading.innerText = factData;

  imageContainer.appendChild(image);
  fact.appendChild(factHeading);

  post.appendChild(imageContainer);
  post.appendChild(fact);

  $(post).animate({ height: '322px' }, 1000);

  $('.main').append(post);
};

export const a = () => 1;
