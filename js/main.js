import { createPost } from './create.js';

const scrollToPost = () => {
  $([document.documentElement, document.body])
  .animate({
    scrollTop: $(".post").last().offset().top,
  }, 1000);
}

const fetchAndCreatePost = async () => {
  try {
    const imageResponse = await fetch('https://aws.random.cat/meow');
    const { file } = await imageResponse.json();
    const factsResponse = await fetch('http://www.catfact.info/api/v1/facts.json');
    const { facts } = await factsResponse.json();
    const randomFactIndex = Math.round(Math.random() * 25);
    const { details: fact } = facts[randomFactIndex];

    createPost(file, fact);
    scrollToPost();
  } catch (error) {
    alert(error);
  }
}

$('#add_post').on('click', fetchAndCreatePost);
